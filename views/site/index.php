<?php
use yii\grid\GridView;
use yii\helpers\Html;

/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
    <h2>Consulta 1</h2>
    
        <p class="lead">Listar con altura menor a 1000 metros figurando el nombre del puertoi y el dorsal del ciclista que lo ganó. </p>

    <?= GridView::widget([
        'dataProvider'=> $resultados,
        'columns'=> $campos
    ]); ?>

    <h2>Consulta 2</h2>

            <p class="lead">Listar estapas no circulares mostrando sólo el número de etapa y la longitud de las mismas. </p>

        <?= GridView::widget([
            'dataProvider'=> $resultados,
            'columns'=> $campos
        ]); ?>
        
    <h2>Consulta 3</h2>

        <p class="lead">Listar los puertos con altura mayor a 1500 metros figurando el nombre del puerto y el dorsal del ciclista que lo ganó. </p>

    <?= GridView::widget([
        'dataProvider'=> $resultados,
        'columns'=> $campos
    ]); ?>

